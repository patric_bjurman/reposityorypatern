INSERT INTO superhero (hero_name, hero_alias, hero_origin)
VALUES ('Wolverine', 'Logan', 'Killed the janitor');

INSERT INTO superhero (hero_name, hero_alias, hero_origin)
VALUES ('Iron Man', 'Tony Stark', 'Creates a mechanized suit of armor while while abducted');

INSERT INTO superhero (hero_name, hero_alias, hero_origin)
VALUES ('Wonder Woman', 'Diana Prince', 'From Themiscyra');
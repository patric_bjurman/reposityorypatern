USE SuperheroesDb;

UPDATE superhero
SET hero_name = 'Doctor Strange', hero_alias = 'Stephen Strange', hero_origin = 'Too vain to accept a teaching job, Strange desperately searches for a way to restore the motor function in his hands.'
WHERE superhero_id = 2;
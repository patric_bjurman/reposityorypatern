USE SuperheroesDb;

CREATE TABLE superhero (
	superhero_id INT NOT NULL IDENTITY PRIMARY KEY (superhero_id),
	hero_name VARCHAR(255),
	hero_alias VARCHAR(255),
	hero_origin VARCHAR(255)
);

CREATE TABLE assistant (
	assistant_id INT NOT NULL IDENTITY PRIMARY KEY (assistant_id),
	assistant_name VARCHAR(255)
);

CREATE TABLE heropower (
	power_id INT NOT NULL IDENTITY PRIMARY KEY (power_id),
	power_name VARCHAR(255),
	power_description VARCHAR(255)
);
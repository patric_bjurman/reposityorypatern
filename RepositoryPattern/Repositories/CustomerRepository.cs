﻿using Microsoft.Data.SqlClient;
using RepositoryPattern.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPattern.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public Customer GetCustomer(int id)
        {
            Customer temp = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.CustomerName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.CustomerLastName = reader.IsDBNull(1) ? null : reader.GetString(2);
                                temp.CustomerCountry = reader.IsDBNull(1) ? null : reader.GetString(3);
                                temp.CustomerPostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.CustomerPhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.CustomerEmail = reader.IsDBNull(1) ? null : reader.GetString(6);
                            }
                        }
                    }

                }
            }
            catch (SqlException)
            {

            }
            return temp;
        }

        public Customer GetCustomerByName(string name)
        {
            Customer temp = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName = @FirstName";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", name);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.CustomerName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.CustomerLastName = reader.IsDBNull(1) ? null : reader.GetString(2);
                                temp.CustomerCountry = reader.IsDBNull(1) ? null : reader.GetString(3);
                                temp.CustomerPostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.CustomerPhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.CustomerEmail = reader.IsDBNull(1) ? null : reader.GetString(6);
                            }
                        }
                    }

                }
            }
            catch (SqlException)
            {

            }
            return temp;
        }

        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer;";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new();
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.CustomerName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.CustomerLastName = reader.IsDBNull(1) ? null : reader.GetString(2);
                                temp.CustomerCountry = reader.IsDBNull(1) ? null : reader.GetString(3);
                                temp.CustomerPostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.CustomerPhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.CustomerEmail = reader.IsDBNull(1) ? null : reader.GetString(6);
                                customerList.Add(temp);
                            }

                        }
                    }

                }
            }
            catch (SqlException)
            {

            }
            return customerList;
        }

        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.CustomerName);
                        cmd.Parameters.AddWithValue("@LastName", customer.CustomerLastName);
                        cmd.Parameters.AddWithValue("@Country", customer.CustomerCountry);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.CustomerPostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.CustomerPhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.CustomerEmail);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException)
            {

            }
            return success;
        }

        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.CustomerName);
                        cmd.Parameters.AddWithValue("@LastName", customer.CustomerLastName);
                        cmd.Parameters.AddWithValue("@Country", customer.CustomerCountry);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.CustomerPostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.CustomerPhoneNumber);
                        cmd.Parameters.AddWithValue("@Email", customer.CustomerEmail);
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException)
            {

            }
            return success;
        }

        public List<Customer> GetPageOfCustomers(int offset, int limit)
        {
            List<Customer> customerList = new();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @OFFSET ROWS FETCH NEXT @LIMIT ROWS ONLY;";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@OFFSET", offset);
                        cmd.Parameters.AddWithValue("@LIMIT", limit);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new();
                                temp.CustomerId = reader.IsDBNull(0) ? 0 : reader.GetInt32(0);
                                temp.CustomerName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                temp.CustomerLastName = reader.IsDBNull(1) ? null : reader.GetString(2);
                                temp.CustomerCountry = reader.IsDBNull(1) ? null : reader.GetString(3);
                                temp.CustomerPostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temp.CustomerPhoneNumber = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temp.CustomerEmail = reader.IsDBNull(1) ? null : reader.GetString(6);
                                customerList.Add(temp);
                            }

                        }
                    }

                }
            }
            catch (SqlException)
            {

            }
            return customerList;
        }

        public List<CustomerCountry> GetCustomersPerCountry()
        {
            List<CustomerCountry> countryList = new();
            string sql = "SELECT Country, COUNT(Country) FROM Customer GROUP BY Country ORDER BY COUNT(Country) desc;";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                CustomerCountry temp = new();
                                temp.CountryName = reader.IsDBNull(0) ? "No country for old men" : reader.GetString(0);
                                temp.CountryCustomers = reader.IsDBNull(1) ? 0 : reader.GetInt32(1);
                                countryList.Add(temp);
                            }

                        }
                    }

                }
            }
            catch (SqlException)
            {

            }
            return countryList;
        }

        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> spenderList = new();
            string sql = "SELECT c.FirstName, SUM(i.Total) FROM Customer c INNER JOIN Invoice i ON c.CustomerId = i.CustomerId GROUP BY c.FirstName, c.CustomerId ORDER BY SUM(i.Total) desc;";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                CustomerSpender temp = new();
                                temp.CustomerSpenderName = reader.IsDBNull(0) ? "No spender for old men" : reader.GetString(0);
                                temp.CustomerTotal = reader.IsDBNull(1) ? 0 : (double)reader.GetDecimal(1);
                                spenderList.Add(temp);
                            }

                        }
                    }

                }
            }
            catch (SqlException)
            {

            }
            return spenderList;
        }

        public List<CustomerGenre> GetCustomersMostPopularGenre(int customerId)
        {
            List<CustomerGenre> customerList = new();
            string sql = "SELECT TOP 1 WITH TIES c.FirstName as Customer_Name, g.Name as Genre_Name FROM((((Customer c INNER JOIN Invoice i ON c.CustomerId = i.CustomerId) INNER JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId) INNER JOIN Track t ON il.TrackId = t.TrackId) INNER JOIN Genre g ON t.GenreId = g.GenreId) WHERE c.CustomerId = @CustomerId GROUP BY c.FirstName, g.Name ORDER BY COUNT(il.TrackId) desc";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make command
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customerId);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                CustomerGenre temp = new();
                                temp.CustomerName = reader.IsDBNull(0) ? "No genre for old men" : reader.GetString(0);
                                temp.CustomerGenreName = reader.IsDBNull(1) ? null : reader.GetString(1);
                                customerList.Add(temp);
                            }
                        }
                    }

                }
            }
            catch (SqlException)
            {

            }
            return customerList;
        }
    }
}

﻿using RepositoryPattern.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPattern.Repositories
{
    public interface ICustomerRepository
    {
        // LISTOR? SNABBARE ATT HÄMTA IFRÅN.

        /// <summary>
        /// Gets a customer specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Customer GetCustomer(int id);

        /// <summary>
        /// Gets a customer specified by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Customer GetCustomerByName(string name);

        /// <summary>
        /// Gets all the customer from the database.
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetAllCustomers();

        /// <summary>
        /// Adds a new customer to the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool AddNewCustomer(Customer customer);

        /// <summary>
        /// Updates a customer in the database.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool UpdateCustomer(Customer customer);

        /// <summary>
        /// Returns a page of customers from the db.
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetPageOfCustomers(int offset, int limit);

        /// <summary>
        /// Get all customers per country. Ordered descending.
        /// </summary>
        public List<CustomerCountry> GetCustomersPerCountry();

        /// <summary>
        /// Get the highest spenders. Based on total in invoice table. Ordered descending.
        /// </summary>
        public List<CustomerSpender> GetHighestSpenders();

        /// <summary>
        /// Gives a certain customers favorite genre.
        /// </summary>
        /// <param name="customer"></param>
        public List<CustomerGenre> GetCustomersMostPopularGenre(int customerId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPattern.Repositories
{
    public interface IRepository<T>
    {
        public T GetEntity(int id);
        public T GetEntityByName(string name);
        public List<T> GetAllEntities();
        public bool AddNewEntity(T entity);
        public bool UpdateEntity(T entity);
        public List<T> GetPageOfEntities(int offset, int limit);

        //public List<T> GetAllEntitiesPerCountry();
    }
}

﻿using RepositoryPattern.Repositories;
using RepositoryPattern.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using RepositoryPattern.Controllers;

namespace RepositoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            ProgramLoop();
        }

        public static void ProgramLoop()
        {
            Console.WriteLine("Connecting to db...");
            CustomerController customerController = new();
            ICustomerRepository repo = new CustomerRepository();
            bool loop = true;
            while (loop)
            {
                int caseSwitch;
                Console.WriteLine("\nHELLO AND WELCOME TO DB. " +
                    "\nWHAT DO YOU WANNA DO? " +
                    "\n1 TO READ ALL CUSTOMERS. " +
                    "\n2 TO READ CUSTOMER BY ID. " +
                    "\n3 TO READ CUSTOMER BY NAME. " +
                    "\n4 TO READ A PAGE OF CUSTOMERS. " +
                    "\n5 TO ADDA NEW CUSTOMER. " +
                    "\n6 TO UPDATE A CUSTOMER. " +
                    "\n7 TO READ CUSTOMERS BY COUNTRY. " +
                    "\n8 TO READ HIGHEST SPENDING CUSTOMERS. " +
                    "\n9 TO READ CUSTOMERS FAVORITE GENRE. " +
                    "\n WRITE 10 TO EXIT. " +
                    "\nPRESS ENTER TO ACTIVATE COMMAND.");
                caseSwitch = Convert.ToInt32(Console.ReadLine());
                switch (caseSwitch)
                {
                    case 1:
                        customerController.ReadAllCustomers(repo);
                        break;
                    case 2:
                        customerController.ReadCustomerById(repo);
                        break;
                    case 3:
                        customerController.ReadCustomerByName(repo);
                        break;
                    case 4:
                        customerController.ReadPageOfCustomers(repo);
                        break;
                    case 5:
                        customerController.AddNewCustomer(repo);
                        break;
                    case 6:
                        customerController.UpdateCustomer(repo);
                        break;
                    case 7:
                        customerController.ReadCustomerByCountry(repo);
                        break;
                    case 8:
                        customerController.ReadHighestSpendingCustomers(repo);
                        break;
                    case 9:
                        customerController.ReadCustomerFavoriteGenre(repo);
                        break;
                    case 10:
                        loop = false;
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("Closing down db...");
        }
    }
}

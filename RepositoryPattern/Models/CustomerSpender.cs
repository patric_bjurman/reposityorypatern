﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPattern.Models
{
    public class CustomerSpender
    {
        public string CustomerSpenderName { get; set; }
        public double CustomerTotal { get; set; }
    }
}

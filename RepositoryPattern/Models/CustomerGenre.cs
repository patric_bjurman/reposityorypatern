﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryPattern.Models
{
    public class CustomerGenre
    {
        public string CustomerGenreName { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}